/* rpi_gpio.h : GPIO access macros and function prototypes */

#ifndef _RPI_GPIO_H_
#define _RPI_GPIO_H_

// 10 pins per function select register and 3 function bits per pin
#define GPFSEL(pin) (((pin)/10)<<2)			//calculate GPFSEL address offset
#define GPFSEL_SHIFT(pin) (((pin)%10)*3)	//calcualte bit-shift to select pin function bits
#define GPFSEL_IN		0					//make pin an input
#define GPFSEL_OUT		1					//make pin an output
#define GPFSEL_MASK	7						//mask used to clear specific pin function bits
// 32 pins per set/clr register
#define GPSET(pin) (((pin)<32)?0x1c:0x20)	//choose address offset for set register
#define GPCLR(pin) (((pin)<32)?0x28:0x2c)	//choose address offset for clr register
#define GPBIT(pin) (1<<((pin)%32))			//bit-shift needed to select pin in set & clr registers

// GPIO pin level registers
#define GPLEV(pin) (((pin)<32)?0x34:0x38)	//choose address offset for register

// GPIO event detect status registers
#define GPEDS0 0x40
#define GPEDS1 0x44
#define GPEDS(pin) (((pin)<32)?0x40:0x44)

// GPIO pin rising edge detect enable 
#define GPREN0 0x4c
#define GPREN1 0x50
#define GPREN(pin) (((pin)<32)?0x4c:0x50)

// GPIO pin falling edge detect enable 
#define GPFEN(pin) (((pin)<32)?0x58:0x5c)

// GPIO pin pull-up/down nnable
#define GPPUD 0x94

// These defines have been replaced by  GetGpioBaseAddr() macro below 
/*
#ifdef __aarch64__
// This is the normal base on 64 bit RPi 3B/3B+/4B
#define PHYSICAL_BASE_ADDRESS 0xfe200000
#elif (__ARM_ARCH==7)
// This is the normal base on 32 bit RPi 2B/3B/3B+/4B
#define PHYSICAL_BASE_ADDRESS 0x3f200000
#else
// This is the normal base on 32 bit RPi 0/0W/A/B/B+
#define PHYSICAL_BASE_ADDRESS 0x20200000
#endif
*/

#define GPIO_BASE_OFFSET 0x200000 // offset from physical peripheral base address
// get physical GPIO base address
#ifdef __aarch64__
#define ARM64 1
#else
#define ARM64 0
#endif
#define GetGpioBaseAddr() ({ \
    u64 gpioBase = 0; \
    do { \
    	u64 mmioBase; \
		register u64 reg; \
    	char *board; \
			\
		if(ARM64) \
    		asm volatile ("mrs %0, midr_el1" : "=r" (reg)); \
		else \
    		asm volatile ("mrc p15, 0, %0, c0, c0, 0" : "=r" (reg)); \
    	switch ((reg >> 4) & 0xFFF) { \
        	case 0xB76: board = "Rpi1"; mmioBase = 0x20000000; break; \
        	case 0xC07: board = "Rpi2"; mmioBase = 0x3F000000; break; \
        	case 0xD03: board = "Rpi3"; mmioBase = 0x3F000000; break; \
        	case 0xD08: board = "Rpi4"; mmioBase = 0xFE000000; break; \
        	default:    board = "????"; mmioBase = 0x20000000; break; \
    	} \
		gpioBase = mmioBase + GPIO_BASE_OFFSET; \
		pr_info("Board = %s, MMIO_BASE = 0x%llx, GPIO_BASE = 0x%llx\n", board, mmioBase, gpioBase); \
	} while(0); \
	gpioBase; \
})

enum gpio_mode {
	GPIO_INPUT,
	GPIO_OUTPUT,
	/* Last GPIO Mode for setting up map */
	GPIO_END = GPIO_OUTPUT
};

int gpio_map(void);
void gpio_unmap(void);
void gpio_set_mode(int pin, enum gpio_mode mode);
void gpio_set_state(int pin, bool on);
bool button_pressed(int pin);
int gpio_irqnum(int pin);
void gpio_ack_interrupts(void);
void gpio_disable_interrupts(void);
void gpio_enable_interrupt(int pin);
bool get_gpio_trigger(int pin);
	
#endif /* _RPI_GPIO_H_ */
