/* rpi_gpio.c : Generic GPIO access functions */

#include <linux/io.h>		//for ioremap(), iounmap()
#include <linux/gpio.h>		//for gpio to linux interrupt number mapping
#include "rpi_gpio.h"

static const int fsel_map[GPIO_END + 1] = {
	GPFSEL_IN,
	GPFSEL_OUT,
};

static u64 gpio_base;
static void __iomem* gpio_virt_base;

static inline u32 readl_offset(ptrdiff_t offset)
{
	return readl(gpio_virt_base + offset);
}

static inline void writel_offset(u32 val, ptrdiff_t offset)
{
	writel(val, gpio_virt_base + offset);
}

int gpio_map(void)
{
	gpio_base = GetGpioBaseAddr(); // get phyiscial base address of gpio unit (see macro in rpi_gpio.h)
	// Map 1 page from physcial address to kernel virtual address space
	// Cast gpio_phys_base to remind that the correct size is phys_addr_t
	gpio_virt_base = ioremap((phys_addr_t)gpio_base, PAGE_SIZE);
	if (IS_ERR(gpio_virt_base)) return -ENOMEM;
	return 0;
}

void gpio_unmap(void)
{
	// Unmap I/O memory from kernel address space
	iounmap(gpio_virt_base);
}

void gpio_set_mode(int pin, enum gpio_mode mode)
{
	// Make pin input or output
	u32 fsel = readl_offset(GPFSEL(pin));
	fsel &= ~(GPFSEL_MASK << GPFSEL_SHIFT(pin));
	fsel |= fsel_map[mode] << GPFSEL_SHIFT(pin);
	writel_offset(fsel, GPFSEL(pin));
}

void gpio_set_state(int pin, bool on)
{
	if (on) writel_offset(GPBIT(pin), GPSET(pin));
	else writel_offset(GPBIT(pin), GPCLR(pin));
}

bool button_pressed(int pin)
{
	bool is_pressed = (readl_offset(GPLEV(pin)) >> pin) & 1;
	return is_pressed;
}

void gpio_ack_interrupts(void)
{
	// clear (acknowledge) all the interrupts by writing FFF...
	writel_offset(~0, GPEDS0);
	writel_offset(~0, GPEDS1);    
}

void gpio_disable_interrupts(void)
{
	writel_offset(0, GPREN0);
	writel_offset(0, GPREN1);
}

void gpio_enable_interrupt(int pin)
{
	writel_offset(1 << pin, GPREN(pin));
}

int gpio_irqnum(int pin)
{
	int irq_num = gpio_to_irq(pin);
	if (irq_num < 0){
		pr_alert(KERN_ALERT "IRQ Number Request Failed\n");
		return -1;
	}
	return irq_num;
}

bool get_gpio_trigger(int pin)
{	
	//readl_offset(GPEDS(pin)) always returns 0, not sure why
	bool triggered = (readl_offset(GPLEV(pin)) >> pin) & 1;
	
	return triggered;
}
