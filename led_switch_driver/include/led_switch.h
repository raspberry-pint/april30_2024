#ifndef _LED_SWTICH_H_
#define _LED_SWITCH_H_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/spinlock.h>
#include <linux/jiffies.h>
#include <linux/miscdevice.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/preempt.h>
#include "rpi_gpio.h"

#define DEVICE_NAME "led_switch_dev"
#define DEFAULT_LED_GPIO 13
#define DEFAULT_BUTTON_GPIO 12
#define DEBOUNCE_DELAY  (200 * HZ / 1000 ) //200 ms delay when HZ = 100

#ifdef DEBUG
#   define PDEBUG(fmt, args...) pr_debug(MODULE_NAME " %s(): "fmt,__func__, ## args)
#else
#   define PDEBUG(fmt, args...) // not debugging, thus print nothing
#endif

// macro to display a thread's context information 
#define PCONTEXT() do {                                             \
	char context[10];                                               \
	if (in_task())                                                  \
		strcpy(context, "process");                                 \
	else if (in_irq())                                              \
		strcpy(context, "hardirq");                                 \
	else if (in_softirq())                                          \
		strcpy(context, "softirq");                                 \
	else                                                            \
		strcpy(context, ".");                                       \
	PDEBUG("(CPU %03d) [%s, %d] context: %s\n",                     \
	    smp_processor_id(),                                         \
	    current->comm, current->pid,                                \
	    context                                                     \
	);                                                              \
} while(0)

// parameters
static int led_gpio = DEFAULT_LED_GPIO;
static int button_gpio = DEFAULT_BUTTON_GPIO;
module_param(led_gpio, int, 0444);
module_param(button_gpio, int, 0444);

MODULE_LICENSE("GPL");

// forward declarations of driver methods
static int led_open(struct inode *inodep, struct file *filep);
static int led_release(struct inode *inode, struct file *file_p);
static ssize_t led_read(struct file *file_p, char __user *user_buf,
                  size_t count, loff_t *offs);
static ssize_t led_write(struct file *file_p, const char __user *user_buf,
				   size_t count, loff_t *offs);

#endif /* _LED_SWITCH_H_ */

