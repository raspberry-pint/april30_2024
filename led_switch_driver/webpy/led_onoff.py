import os

LED_FILE = '/dev/led_switch_dev'

def query():
    fd = os.open(LED_FILE, os.O_RDWR)
    buf = os.read(fd, 1)
    os.close(fd)
    if buf == b'1':
        return 'ON'
    else:
        return 'OFF'

def turn_on():
    fd = os.open(LED_FILE, os.O_RDWR)
    # '1' needs to be converted byte object
    byte = str.encode('1')
    os.write(fd, byte)
    os.close(fd)

def turn_off():
    fd = os.open(LED_FILE, os.O_RDWR)
    # '0' needs to be converted byte object
    byte = str.encode('0')
    os.write(fd, byte)
    os.close(fd)
