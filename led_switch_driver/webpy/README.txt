This is a Python implementation of a simple web page that allows a user to
check and toggle the led

To start:
    ./controller.py

You can access the web page using by navigating to:
    http://<rpi_ip_address>:5000

Please note that any device on the network can do this making this insecure.
Use at own risk.
